import Vuex from 'vuex'

const store = () => new Vuex.Store({
  state: {
    user: null
  },
  mutations: {
    mutate_auth_userData(state, value) {
      state.user = value
    }
  },
  actions: {
    /* auth setup */
    act_auth_setupUserData({commit}, user) {
      commit('mutate_auth_userData', user)
    },
    act_auth_logout({commit}) {
      commit('mutate_auth_userData', null)
      this.$cookies.remove('authToken')
    },
    async nuxtServerInit ({ dispatch }, { app, $axios }) {
      if ( app.$cookies.get('authToken') ) {
        const { data } = await $axios.get('/auth/init')
        dispatch('act_auth_setupUserData', data.user)
      }
    }
  }
})

export default store
