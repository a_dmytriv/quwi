export default ({ app, $axios, store }, inject) => {
  $axios.interceptors.request.use((config) => {
    let token = app.$cookies.get('authToken')
    // intercept headers for auth
    if (token) {
      config.headers = { Authorization: `Bearer ${token}` }
    }
    return config;
  });
}
