/* global beforeEach route middleware */
export default function(context) {
  /* check if jwt exist in $cookies */
  if (!context.app.$cookies.get('authToken') && context.route.path !== '/login') {
    context.redirect('/login')
  }
}
